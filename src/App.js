import React, { Component } from "react";
import Ehsplayer from "./Ehsplayer";

class App extends Component {
  constructor() {
    super();
    this.state = {
      options: {
        // autoplay: true,

        controls: true,
        sources: [
          {
            src: "http://www.html5videoplayer.net/videos/toystory.mp4"
          }
        ]
      },
      eventLister: {
        pause: function(e, video) {
          console.log("暫停");
        },
        play: function(e, video) {
          console.log("播放");
        }
      }
    };
  }

  render() {
    return (
      <div className="app">
        <Ehsplayer
          options={this.state.options}
          eventLister={this.state.eventLister}
        />
      </div>
    );
  }
}

export default App;
