import React, { Component } from "react";

class Ehsplayer extends Component {
  constructor(props) {
    super(props);
    this.player = React.createRef();
  }

  render() {
    let { options } = this.props;
    return (
      <React.Fragment>
        <video
          className="video-js vjs-big-play-centered vjs-ehs"
          data-setup={JSON.stringify(options)}
          ref={this.player}
        />
      </React.Fragment>
    );
  }
  componentDidMount() {
    const domPlayer = this.player.current;
    let { eventLister } = this.props;
    this.video = window.videojs(domPlayer, {
      controls: true,
      autoplay: false,
      preload: "auto"
    });

    // 監聽事件
    console.log(this.props)
    Object.keys(eventLister).forEach(item => {
      this.video.on(item, e => {
        eventLister[item](e, this.video);
      });
    });
  }

  componentWillUnmount() {
    console.log("播放器元件消除");
    this.video.dispose();
  }
}

export default Ehsplayer;
